%global relnum 29
%global Bg_Name Unity-Linux
%global bgname unity

# Enable Animation
%global with_animated 0

# Enable Extras
%global with_extras 1

Name:           %{bgname}-backgrounds
Version:        %{relnum}.0.0
Release:        1%{?dist}
Summary:        Unity-Linux default desktop background

License:        CC-BY-SA
URL:            https://gitlab.com/unity-linux/unity-backgrounds
Source0:        %{name}-%{version}.tar.xz
# Plasma desktoptheme
# This is in the source tarball
#Source1:        metadata.desktop

BuildArch:	noarch

# for %%_kde4_* macros
BuildRequires:	kde-filesystem

Requires:	%{name}-gnome = %{version}-%{release}
Requires:	%{name}-kde = %{version}-%{release}
Requires:	%{name}-xfce = %{version}-%{release}
Requires:	%{name}-mate = %{version}-%{release}

Provides:       f%{relnum}-backgrounds
Obsoletes:      f%{relnum}-backgrounds

%description
This package contains desktop backgrounds for the Unity-Linux %{relnum} default
theme. Pulls in themes for GNOME, KDE, Mate and Xfce desktops.

%package	base
Summary:	Base images for Unity-Linux %{relnum} default background
License:	CC-BY-SA

Provides:       f%{relnum}-backgrounds-base
Obsoletes:      f%{relnum}-backgrounds-base

%description	base
This package contains base images for Unity-Linux %{relnum} default background.

%if %{with_animated}
%package	animated
Summary:	Time of day images for Fedora %{relnum} backgrounds

Requires:	%{name}-base = %{version}-%{release}

%description	animated
This package contains the time of day images for Fedora %{relnum}
Backgrounds.
%endif

%package	kde
Summary:	Unity-Linux %{relnum} default wallpaper for KDE

Requires:	%{name}-base = %{version}-%{release}
Requires:	kde-filesystem
Supplements:	%{name}-animated = %{version}-%{release}

Provides:       f%{relnum}-backgrounds-kde
Obsoletes:      f%{relnum}-backgrounds-kde

%description    kde
This package contains KDE desktop wallpaper for the Unity-Linux %{relnum}
default theme.

%package	gnome
Summary:	Unity-Linux %{relnum} default wallpaper for Gnome and Cinnamon

Requires:	%{name}-base = %{version}-%{release}
Supplements:	%{name}-animated = %{version}-%{release}

Provides:       f%{relnum}-backgrounds-gnome
Obsoletes:      f%{relnum}-backgrounds-gnome

%description	gnome
This package contains GNOME/Cinnamon desktop wallpaper for the
Unity-Linux %{relnum} default theme.

%package	mate
Summary:	Unity-Linux %{relnum} default wallpaper for Mate

Requires:	%{name}-base = %{version}-%{release}
Supplements:	%{name}-animated = %{version}-%{release}

Provides:       f%{relnum}-backgrounds-mate
Obsoletes:      f%{relnum}-backgrounds-mate

%description	mate
This package contains MATE desktop wallpaper for the Unity-Linux %{relnum}
default theme.

%package	xfce
Summary:	Unity-Linux default background for Xfce4

Requires:	%{name}-base = %{version}-%{release}
Requires:	xfdesktop

Provides:       f%{relnum}-backgrounds-xfce
Obsoletes:      f%{relnum}-backgrounds-xfce

%description	xfce
This package contains Xfce4 desktop background for the Unity-Linux
default theme.

%if %{with_extras}
%package	extras-base
Summary:	Base images for Unity-Linux Extras Backrounds
License:	CC-BY and CC-BY-SA
Provides:       f%{relnum}-backgrounds-extras-base
Obsoletes:      f%{relnum}-backgrounds-extras-base

%description	extras-base
This package contains base images for Unity-Linux supplemental
wallpapers.

%package	extras-gnome
Summary:	Extra Unity-Linux Wallpapers for GNOME and Cinnamon

Requires:	%{name}-extras-base

Provides:       f%{relnum}-backgrounds-extras-gnome
Obsoletes:      f%{relnum}-backgrounds-extras-gnome

%description	extras-gnome
This package contains Unity-Linux supplemental wallpapers for GNOME
and Cinnamon

%package	extras-mate
Summary:	Extra Unity-Linux Wallpapers for MATE

Requires:	%{name}-extras-base = %{version}-%{release}

Provides:       f%{relnum}-backgrounds-extras-mate
Obsoletes:      f%{relnum}-backgrounds-extras-mate

%description    extras-mate
This package contains Unity-Linux supplemental wallpapers for MATE

%package	extras-kde
Summary:	Extra Unity-Linux Wallpapers for KDE

Requires:	%{name}-extras-base

Provides:       f%{relnum}-backgrounds-extras-kde
Obsoletes:      f%{relnum}-backgrounds-extras-kde

%description	extras-kde
This package contains Unity-Linux supplemental wallpapers for GNOME

%package	extras-xfce
Summary:	Extra Unity-Linux Wallpapers for Xfce

Requires:	%{name}-extras-base = %{version}-%{release}

Provides:       f%{relnum}-backgrounds-extras-xfce
Obsoletes:      f%{relnum}-backgrounds-extras-xfce

%description	extras-xfce
This package contains Unity-Linux supplemental wallpapers for Xfce
%endif

%prep
%autosetup


%build
%make_build


%install
%make_install

install -D -p -m644 metadata.desktop \
%{buildroot}%{_datadir}/plasma/desktoptheme/%{Bg_Name}/metadata.desktop

%files
%doc

%files base
%license CC-BY-SA-4.0 Attribution
%dir %{_datadir}/backgrounds/%{bgname}
%dir %{_datadir}/backgrounds/%{bgname}/default
%{_datadir}/backgrounds/%{bgname}/default/normalish
%{_datadir}/backgrounds/%{bgname}/default/standard
%{_datadir}/backgrounds/%{bgname}/default/wide
%{_datadir}/backgrounds/%{bgname}/default/tv-wide
%{_datadir}/backgrounds/%{bgname}/default/%{bgname}.xml

%if %{with_animated}
%files animated
%dir %{_datadir}/backgrounds/%{bgname}/default-animated
%{_datadir}/backgrounds/%{bgname}/default-animated/normalish
%{_datadir}/backgrounds/%{bgname}/default-animated/standard
%{_datadir}/backgrounds/%{bgname}/default-animated/wide
%{_datadir}/backgrounds/%{bgname}/default-animated/tv-wide
%{_datadir}/backgrounds/%{bgname}/default-animated/%{bgname}.xml
%endif

%files kde
%{_kde4_datadir}/wallpapers/%{Bg_Name}/
%dir %{_datadir}/plasma/
%dir %{_datadir}/plasma/desktoptheme/
%{_datadir}/plasma/desktoptheme/%{Bg_Name}/

%files gnome
%{_datadir}/gnome-background-properties/%{bgname}.xml
%if %{with_animated}
%{_datadir}/gnome-background-properties/%{bgname}-animated.xml
%endif

%files mate
%{_datadir}/mate-background-properties/%{bgname}.xml
%if %{with_animated}
%{_datadir}/mate-background-properties/%{bgname}-animated.xml
%endif

%files xfce
%{_datadir}/xfce4/backdrops/%{bgname}.png

%if %{with_extras}
%files extras-base
%license CC-BY-SA-4.0 CC-BY-4.0 CC0-1.0 Attribution-Extras
%{_datadir}/backgrounds/%{bgname}/extras/*.jpg
%{_datadir}/backgrounds/%{bgname}/extras/*.png
%{_datadir}/backgrounds/%{bgname}/extras/%{bgname}-extras.xml

%files extras-gnome
%{_datadir}/gnome-background-properties/%{bgname}-extras.xml

%files extras-kde
%{_kde4_datadir}/wallpapers/%{Bg_Name}_*/

%files extras-mate
%{_datadir}/mate-background-properties/%{bgname}-extras.xml

%files extras-xfce
%{_datadir}/xfce4/backdrops/*.jpg
%{_datadir}/xfce4/backdrops/*.png
%endif

%changelog
* Thu Oct 18 2018 JMiahMan <jmiahman@unity-linux.com> - 29.0-1
- Use this package for Unity for now

* Tue Oct 10 2017 Ian Firns <firnsy@kororaproject.org> - 27.0-1
- Update for Korora 27
